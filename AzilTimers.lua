﻿if not (Tukui or AsphyxiaUI or DuffedUI or IsAddOnLoaded('ElvUI')) then return end
local A, C, _, G = unpack(Tukui or DuffedUI or AsphyxiaUI or IsAddOnLoaded('ElvUI') and ElvUI)
local AddOnName, Engine = ...
local EP = LibStub('LibElvUIPlugin-1.0')
local LSM = LibStub('LibSharedMedia-3.0')

AzilTimersOptions = {}

local DebuffList = {
	['DEATHKNIGHT'] = {
		55078, -- Blood Plague
		55095, -- Frost Fever
		73975, -- Necrotic Strike
		115798, -- Weakened Blows
	},
	['DRUID'] = {
		1079, -- Rip
		1822, -- Rake
		8921, -- Moonfire
		9007, -- Pounce Bleed
		33745, -- Lacerate
		45334, -- Immobilized
		58180, -- Infected Wounds
		77758, -- Thrash
		82365, -- Skull Bash
		93402, -- Sunfire
		115798, -- Weakened Blows
	},
	['HUNTER'] = {
		1130, -- Hunter's Mark
		1978, -- Serpent Sting
		3674, -- Black Arrow
		4167, -- Web (Spider)
		53301, -- Explosive Shot
		54706, -- Venom Web Spray (Silithid)
		50245, -- Pin (Crab)
	},
	['MAGE'] = {
		120, -- Cone of Cold
		122, -- Frost Nova
		11366, -- Pyroblast
		12654, -- Ignite
		31589, -- Slow
		33395, -- Freeze (Pet)
		36032, -- Arcane Charge
		44457, -- Living Bomb
		44614, -- Frostfire Bolt
		83853, -- Combustion
		111340, -- Ice Ward
		112948, -- Frost Bomb
		114923, -- Nether Tempest
	},
	['MONK'] = {
		116330, -- Dizzying Haze
		123725, -- Breath of Fire
		124273, -- Heavy Stagger
	},
	['PALADIN'] = {
		2812, -- Denounce
		20170, -- Seal of Justice
		25771, -- Forbearance
		31803, -- Censure
		63529, -- Glyph of Dazing Shield
		110300, -- Burden of Guilt
		115798, -- Weakened Blows
	},
	['PRIEST'] = {
		589, -- Shadow Word: Pain
		2944, -- Devouring Plague
		34914, -- Vampiric Touch
		87194, -- Glyph of Mind Blast
		114404, -- Void Tendril's Grasp
	},
	['ROGUE'] = {
		408, -- Kidney Shot
		703, -- Garrote
		1776, -- Gouge
		1833, -- Cheap Shot
		1943, -- Rupture
		2094, -- Blind
		2818, -- Deadly Poison
		3409, -- Crippling Poison
		5760, -- Mind-numbing Poison
		6770, -- Sap
		8680, -- Wound Poison
		16511, -- Hemmorrhage
		51722, -- Dismantle
		79140, -- Vendetta
		84617, -- Revealing Strike
		91021, -- Find Weakness
		113746, -- Weakened Armor
	},
	['SHAMAN'] = {
		8050, -- Flame Shock
		8056, -- Frost Shock
		17364, -- Stormstrike
		64695, -- Earthgrab	
		77661, -- Searing Flames
	},
	['WARLOCK'] = {
		1098, -- Enslave Demon
		1490, -- Curse of the Elements
		18223, -- Curse of Exhaustion
		63311, -- Shadowsnare
		109466, -- Curse of Enfeeblement
	},
	['WARRIOR'] = {
		1160, -- Demoralizing Shout
		1715, -- Hamstring
		86346, -- Colossus Smash
		113746, -- Weakened Armor
		115798, -- Weakened Blows
	},
}

local BuffList = {
	['DEATHKNIGHT'] = {
		77535, -- Blood Shield
		51271, -- Pillar of Frost
	},
	['DRUID'] = {
		62606, -- Savage Defense
	},
	['HUNTER'] = {
	},
	['MAGE'] = {
	},
	['MONK'] = {
	},
	['PALADIN'] = {
	},
	['PRIEST'] = {
	},
	['ROGUE'] = {
		5171, -- Slice and Dice
		84745, -- Shallow Insight
		84746, -- Moderate Insight
		84747, -- Deep Insight
		115189, -- Anti
	},
	['SHAMAN'] = {
	},
	['WARLOCK'] = {
	},
	['WARRIOR'] = {
	},
	-- Trinkets / Enchants
	['SHARED'] = {
		122316,
	},
}

local DebuffTimers = {}
local DebuffFrames = {}

local BuffTimers = {}
local BuffFrames = {}

AzilTimers = {}

local MyClass = select(2, UnitClass('player'))
local Title = select(2, GetAddOnInfo(AddOnName))
local Version = GetAddOnMetadata(AddOnName, 'Version')

DebuffFrames = DebuffList[MyClass]
BuffFrames = BuffList[MyClass]
for i = 1, #BuffList['SHARED'] do
	tinsert(BuffFrames, BuffList['SHARED'][i])
end

function AzilTimers:EnableTimer(self)
	self.Enabled = true
	self:Show()
	self:SetAlpha(1)
	self:EnableMouse(true)
	local BorderColor = IsAddOnLoaded('ElvUI') and A['media'].bordercolor or C['media'].bordercolor
	self:SetScript('OnUpdate', function(self, elapsed)
		local _, _, Icon, Count, _, Start, Finish, _, _, _, _, _, _, Value1, Value2, Value3 = UnitAura(self.SpellUnit, self.SpellName, nil, self.SpellFilter)
		if Duration and Duration > 1.5 then
			local CurrentDuration = Expires - GetTime()
			local Normalized = CurrentDuration / Duration
			self.StatusBar:SetValue(Normalized)
			self.DurationText:SetTextColor(1, 1, 0)
			local Backdrop = self.backdrop or self.Backdrop
			local StatusBarBackdrop = self.StatusBar.backdrop or self.StatusBar.Backdrop
			Backdrop:SetBackdropBorderColor(unpack(BorderColor))
			StatusBarBackdrop:SetBackdropBorderColor(unpack(BorderColor))
			if CurrentDuration > 60 then
				self.DurationText:SetFormattedText('%dm', ceil(CurrentDuration / 60))
			elseif CurrentDuration <= 60 and CurrentDuration > 10 then
				self.DurationText:SetFormattedText('%d', CurrentDuration)
			elseif CurrentDuration <= 10 and CurrentDuration > 0 then
				self.DurationText:SetTextColor(1, 0, 0)
				self.DurationText:SetFormattedText('%.1f', CurrentDuration)
				Backdrop:SetBackdropBorderColor(1, 0, 0)
				StatusBarBackdrop:SetBackdropBorderColor(1, 0, 0)
			end
			if Count ~= 0 then self.CountText:SetText(Count) end
			if self.SpellID == 77535 then
				self.CountText:SetText(select(4, UnitBuff('player', GetSpellInfo(50421), nil, nil)))
				self.ValueText:SetText(Value2)
			end
			if AzilTimersOptions['FadeGreenRed'] then self.StatusBar:SetStatusBarColor(1 - Normalized, Normalized, 0) end
			if AzilTimersOptions['FadeRedGreen'] then self.StatusBar:SetStatusBarColor(Normalized, 1 - Normalized, 0) end
			if AzilTimersOptions['StatusBar'] then
				self.StatusBar:Show()
				self.DurationText:Show()
				self.Cooldown:Hide()
			else
				self.StatusBar:Hide()
				self.DurationText:Hide()
				self.Cooldown:Show()
				Aura.Cooldown:SetCooldown(Finish - Start, Start)
			end
		else
			if self.Enabled then
				AzilTimers:DisableTimer(self)
			end
		end
	end)
end

function AzilTimers:DisableTimer(self)
	self.Enabled = false
	self:Hide()
	self.StatusBar:Show()
	self.DurationText:Show()
	self.Cooldown:Show()
	self:SetScript('OnUpdate', nil)
end

function AzilTimers:CreateTimerFrame(SpellID, IsDebuff)
	local Parent = IsDebuff and AzilDebuffTimersFrame or AzilBuffTimersFrame
	local Frame = CreateFrame('Button', 'AzilTimer_'..SpellID, Parent)
	Frame:RegisterForClicks('AnyDown')
	Frame:CreateBackdrop()
	Frame:SetSize(AzilTimersOptions['Size'], AzilTimersOptions['Size'])

	Frame.Icon = Frame:CreateTexture(nil, 'ARTWORK')
	Frame.Icon:SetAllPoints()
	Frame.Icon:SetTexCoord(.08, .92, .08, .92)

	Frame.DurationText = Frame:CreateFontString(nil, 'OVERLAY')
	Frame.DurationText:SetTextColor(1, 1, 0)
	Frame.DurationText:SetPoint('BOTTOMLEFT', Frame, 'BOTTOMLEFT', 1, 1)

	Frame.StatusBar = CreateFrame('StatusBar', nil, Frame)
	Frame.StatusBar:CreateBackdrop('Transparent')
	Frame.StatusBar:SetPoint('TOP', Frame, 'BOTTOM', 0, (ElvUI and ElvUI[1].PixelMode and -3 or -5))
	Frame.StatusBar:SetMinMaxValues(0, 1)

	Frame.Cooldown = CreateFrame('Cooldown', nil, Frame, 'CooldownFrameTemplate')
	Frame.Cooldown:SetAllPoints(Frame)

	Frame.ValueText = Frame:CreateFontString(nil, 'OVERLAY')
	Frame.ValueText:Point('TOPRIGHT', Frame, 'TOPRIGHT', 0, 0)

	Frame.CountText = Frame:CreateFontString(nil, 'OVERLAY')
	Frame.CountText:SetPoint('BOTTOMRIGHT', Frame, 'BOTTOMRIGHT', 0, 1)

	Frame.SpellID = SpellID
	Frame.SpellName = GetSpellInfo(SpellID)
	Frame.SpellFilter = 'HELPFUL|PLAYER'
	Frame.SpellUnit = 'player'
	if IsDebuff then
		Frame.Debuff = true
		Frame.SpellUnit = 'target'
		if SpellID == (113746 or 115798 or 25771) then
			Frame.SpellFilter = 'HARMFUL'
		else
			Frame.SpellFilter = 'HARMFUL|PLAYER'
		end
	end
	AzilTimers:DisableTimer(Frame)
	return Frame
end

local function CreateMover(Name, Text)
	local Frame = CreateFrame('Frame', Name, UIParent)
	Frame:Hide()
	Frame:Size(120, 50)
	Frame:SetFrameStrata('HIGH')
	Frame:SetTemplate()
	Frame:SetBackdropBorderColor(1, 0, 0)
	Frame:SetMovable()
	Frame:FontString('Text', C['media'].uffont, 14)
	Frame.Text:SetText(Text)
	Frame.Text:SetPoint('CENTER')
	if Tukui or DuffedUI then
		tinsert(A.AllowFrameMoving, Frame)
		hooksecurefunc(A, 'MoveUIElements', function() if _G[Name]:IsShown() then _G[Name]:Hide() else _G[Name]:Show() end end)
	end
end

function AzilTimers:GetOptions()
	local Ace3OptionsPanel = IsAddOnLoaded('ElvUI') and ElvUI[1] or Enhanced_Config[1]
	Ace3OptionsPanel.Options.args.AzilTimers = {
		type = 'group',
		name = Title,
		order = -101,
		args = {
			main = {
				order = 1,
				type = 'group',
				name = 'Main',
				guiInline = true,
				get = function(info) return AzilTimersOptions[info[#info]] end,
    			set = function(info, value) AzilTimersOptions[info[#info]] = value end, 
				args = {
					Vertical = {
						order = 1,
						type = 'toggle',
						name = 'Vertical',
					},
					Size = {
						order = 2,
						type = 'range',
						width = 'full',
						name = 'Icon Size',
						min = 30, max = 60, step = 1,
					},
					Spacing = {
						order = 3,
						type = 'range',
						width = 'full',
						name = 'Icon Spacing',
						min = 0, max = 20, step = 1,
					},
					StatusBar = {
						order = 5,
						type = 'toggle',
						name = 'StatusBar',
					},
					StatusBarTexture = {
						type = 'select', dialogControl = 'LSM30_Statusbar',
						order = 6,
						name = 'StatusBar Texture',
						values = AceGUIWidgetLSMlists.statusbar,
						disabled = function() return not AzilTimersOptions['StatusBar'] end,
					},
					FadeGreenRed = {
						order = 7,
						type = 'toggle',
						name = 'Fade Green to Red',
						disabled = function() return not AzilTimersOptions['StatusBar'] or AzilTimersOptions['FadeRedGreen'] end,
					},
					FadeRedGreen = {
						order = 8,
						type = 'toggle',
						name = 'Fade Red to Green',
						disabled = function() return not AzilTimersOptions['StatusBar'] or AzilTimersOptions['FadeGreenRed'] end,
					},
					StatusBarTextureColor = {
						type = 'color',
						order = 9,
						name = 'StatusBar Texture Color',
						hasAlpha = false,
						get = function(info) local t = AzilTimersOptions[info[#info]] return t.r, t.g, t.b, t.a end,
						set = function(info, r, g, b) AzilTimersOptions[info[#info]] = {} local t = AzilTimersOptions[info[#info]] t.r, t.g, t.b = r, g, b end,	
						disabled = function() return not AzilTimersOptions['StatusBar'] or AzilTimersOptions['FadeGreenRed'] or AzilTimersOptions['FadeRedGreen'] end,
					},
					DurationFont = {
						type = 'select', dialogControl = 'LSM30_Font',
						order = 10,
						name = 'Duration Font',
						values = AceGUIWidgetLSMlists.font,
						disabled = function() return not AzilTimersOptions['StatusBar'] end,
					},
					DurationFontSize = {
						order = 11,
						name = 'Duration Font Size',
						type = 'range',
						min = 8, max = 22, step = 1,
						disabled = function() return not AzilTimersOptions['StatusBar'] end,
					},
					DurationFontFlag = {
						name = 'Duration Font Flag',
						order = 12,
						type = 'select',
						values = {
							['NONE'] = 'None',
							['OUTLINE'] = 'OUTLINE',
							['MONOCHROME'] = 'MONOCHROME',
							['MONOCHROMEOUTLINE'] = 'MONOCROMEOUTLINE',
							['THICKOUTLINE'] = 'THICKOUTLINE',
						},
						disabled = function() return not AzilTimersOptions['StatusBar'] end,
					},
  				},
			},
		},
	}
end

local AzilTimersLoader = CreateFrame('Frame')
AzilTimersLoader:RegisterEvent('ADDON_LOADED')
AzilTimersLoader:RegisterEvent('PLAYER_ENTERING_WORLD')
AzilTimersLoader:SetScript('OnEvent', function(self, event, addon)
	if addon == AddOnName then
		if AzilTimersOptions['Spacing'] == nil then AzilTimersOptions['Spacing'] = 10 end
		if AzilTimersOptions['Size'] == nil then AzilTimersOptions['Size'] = 36 end
		if AzilTimersOptions['Vertical'] == nil then AzilTimersOptions['Vertical'] = false end
		if AzilTimersOptions['StatusBar'] == nil then AzilTimersOptions['StatusBar'] = true end
		if AzilTimersOptions['StatusBarTexture'] == nil then AzilTimersOptions['StatusBarTexture'] = 'Asphyxia' end
		if AzilTimersOptions['StatusBarTextureColor'] == nil then AzilTimersOptions['StatusBarTextureColor'] = { ['r'] = .24, ['g'] = .54, ['b'] = .78 } end
		if AzilTimersOptions['DurationFont'] == nil then AzilTimersOptions['DurationFont'] = 'Tukui Pixel' end
		if AzilTimersOptions['DurationFontSize'] == nil then AzilTimersOptions['DurationFontSize'] = 12 end
		if AzilTimersOptions['DurationFontFlag'] == nil then AzilTimersOptions['DurationFontFlag'] = 'MONOCHROMEOUTLINE' end

		local function CreateTimerFrame(Name, Parent, Offset, Timers, Frames, IsDebuff)
			local Frame = CreateFrame('Frame', Name, UIParent)
			Frame:SetFrameStrata('BACKGROUND')
			Frame:SetSize(40, 40) 
			if ElvUI then
				Frame:Point('BOTTOM', ElvUI[1].UIParent, 'BOTTOM', 0, Offset)
			else
				Frame:Point('TOP', Parent, 'TOP', 0, -2)
			end
			Frame.Timers = Timers
			Frame.Frames = Frames
			Frame.IsDebuff = IsDebuff
			Frame.Position = function()
				local Vertical, Spacing, Size = AzilTimersOptions['Vertical'], AzilTimersOptions['Spacing'], AzilTimersOptions['Size']
				local xSpacing = Vertical and 0 or Spacing
				local ySpacing = Vertical and -(Spacing + (AzilTimersOptions['StatusBar'] and 8 or 0)) or 0
				local AnchorPoint = Vertical and 'BOTTOMLEFT' or 'TOPRIGHT'
				local LastFrame = _G[Name]
				local Index = 0
				local Color = AzilTimersOptions['StatusBarTextureColor']
				for k, v in pairs(Frame.Timers) do
					local Frame = Frame.Timers[k]
					local _, _, Icon, Count = UnitAura(Frame.SpellUnit, Frame.SpellName, nil, Frame.SpellFilter)
					Frame:Size(Size)
					Frame.StatusBar:SetSize(AzilTimersOptions['Size'], 4)
					Frame.DurationText:SetFont(LSM:Fetch('font', AzilTimersOptions['DurationFont']), AzilTimersOptions['DurationFontSize'], AzilTimersOptions['DurationFontFlag'])
					Frame.StatusBar:SetStatusBarTexture(LSM:Fetch('statusbar', AzilTimersOptions['StatusBarTexture']))
					Frame.StatusBar:SetStatusBarColor(Color['r'], Color['g'], Color['b'])
					if Duration and Duration > 1.5 then
						Frame.Icon:SetTexture(Icon)
						Frame:ClearAllPoints()
						Frame:Point('TOPLEFT', LastFrame, Index == 0 and 'TOPLEFT' or AnchorPoint, xSpacing, ySpacing)
						if not Frame.Enabled then
							AzilTimers:EnableTimer(Frame)
						end
						LastFrame = Frame
						Index = Index + 1
					else
						if Frame.Enabled then
							AzilTimers:DisableTimer(Frame)
						end
					end
				end
				if Vertical then
					_G[Name]:Height(Size * Index + (Index + 1) * ySpacing)
				else
					_G[Name]:Width(Size * Index + (Index + 1) * xSpacing)
				end
			end
			Frame:RegisterEvent('PLAYER_LOGIN')
			Frame:RegisterUnitEvent('UNIT_AURA', 'player', 'target')
			Frame:RegisterEvent('SPELL_UPDATE_USABLE')
			Frame:RegisterEvent('PLAYER_TARGET_CHANGED')
			Frame:SetScript('OnEvent', function(self, event)
				if event == 'PLAYER_LOGIN' then
					for k, v in pairs(self.Frames) do
						self.Timers[k] = self.Timers[k] or AzilTimers:CreateTimerFrame(self.Frames[k], self.IsDebuff)
					end
					self.Position()
				elseif event == 'SPELL_UPDATE_USABLE' or event == 'UNIT_AURA' or event == 'PLAYER_TARGET_CHANGED' then
					self.Position()
				end
			end)
		end

		if not ElvUI then
			CreateMover('AzilDebuffTimersMover', 'AzilDebuffTimers')
			AzilDebuffTimersMover:SetPoint('BOTTOM', UIParent, 'BOTTOM', 0, 240)
			CreateMover('AzilBuffTimersMover', 'AzilBuffTimers')
			AzilBuffTimersMover:SetPoint('BOTTOM', UIParent, 'BOTTOM', 0, 300)
		end

		CreateTimerFrame('AzilDebuffTimersFrame', 'AzilDebuffTimersMover', 240, DebuffTimers, DebuffFrames, true)
		CreateTimerFrame('AzilBuffTimersFrame', 'AzilBuffTimersMover', 300, BuffTimers, BuffFrames, false)
	end
	if event == 'PLAYER_ENTERING_WORLD' then
		if IsAddOnLoaded('ElvUI') then
			ElvUI[1]:CreateMover(AzilBuffTimersFrame, 'AzilBuffTimersMover', 'AzilBuffs Anchor', nil, nil, nil, 'ALL,GENERAL')
			ElvUI[1]:CreateMover(AzilDebuffTimersFrame, 'AzilDebuffTimersMover', 'AzilDebuffs Anchor', nil, nil, nil, 'ALL,GENERAL')
			EP:RegisterPlugin(AddOnName, AzilTimers.GetOptions)
		else
			AzilTimers:GetOptions()
		end
		print(format('%s by |cFFFF7D0AAzilroka|r - Version: |cff1784d1%s|r Loaded!', Title, Version))
	end
	self:UnregisterEvent(event)
end)